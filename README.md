# aCMSTS

_**antiX Community Multilanguage Support Test Suite (aCMSTS)**_ allows fast testing and editing of language files present on a running antiX system. It is meant to test translated strings in situ before supplying to transifex. Also for provisional fixes.

Questions, suggestions and bug reports please to [antiX linux forum](forum.antiXlinux.com).

**Installation:**

- [ ] Download most recent aCMSTS installer archive file from _installer-packages_ subdirectory and extract it to a folder of your choice.
- [ ] In bash command line (e.g. terminal emulation program) move to folder containing content of the extracted archive and execute installer.sh.

Aditional hints:
- You may have to prepend _/bin/bash_ and/or _sudo_ to the install command.
- Experienced users may install aCMSTS manually by copying all files from extracted installer-package ressources folder to the designated system directories.
- Tested to be functional on antiX 17.4.1 and antiX 19.x

**Usage:**

Start aCMSTS from its menu entry in _antiX main menu_, submenu _applications/development_. Follow the instructions in aCMSTS dialog windows. Click _help button_ in first dialog for more information.

Please transfer modified strings after testing with aCMSTS to transifex (either [antiX-development](https://www.transifex.com/anticapitalista/antix-development/dashboard/) or [antiX-Community-Contributions](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/dashboard/), dependent on the program whose translation string is to be modified, corrected or addded)in order to make them permanent. **Otherwise you may lose your modifications on next antiX system or involved program update**.

---------------------------

This Program is still under developement, but can be used already.
